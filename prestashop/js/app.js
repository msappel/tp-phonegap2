;$(function(){ //DOM ready

	const URL_PROXY = 'http://golivier.srv5.formations-web.alsace/webservices-2016-18/prestashop/proxy_prestashop.php';

	window.addEventListener("offline", function () {
		alert('JE suis offline');
	}, false);
	window.addEventListener("online", function () {
		alert('JE suis online');
	}, false);


	if(navigator.onLine)
	{

		$.ajax({
			url : URL_PROXY,
			data : {
				action : 'tree',
			}	
		}).done(function(response){
			console.log(response);
			
			$.each(response.categories,function(i,category) {
				
				if(category.id_parent == 0)
				{
					//c'est la category racine
					localStorage.setItem('id_main_category' , category.id);
				}
				
				localStorage.setItem('cat_' + category.id, JSON.stringify(category));
				
				
			});
			
			_drawMenu();
			
		}).fail(function(){
			alert('Proxy injoignable');
		});
	
	}
	else //je ne suis pas connecté, mais je peux dessiner le menu a partir du cache
	{
		_drawMenu();	
	}
	
	
	var _drawMenu = function() {
		//recuperation de l'id de la categorie principale
		var id_main_category = localStorage.getItem('id_main_category');
		var main_category = _getCategoryFromLocalStorage(id_main_category);
		
		if(main_category !== false)
		{
			$.each(main_category.subcategories,function(i,id_category){
				_drawNode(id_category, $('#menu'));
			});
		}
		else
		{
			alert('Désolé, aucune information stockée en cache actuellement, veuillez vous connecter à internet.');
		}
		
	};
	
	var _drawNode = function(id_category, $container)
	{
		var category = _getCategoryFromLocalStorage(id_category);
		if(category !== false)
		{
			var favorite = '';
			
			if(localStorage.getItem('fav_' + id_category) != null)
			{
				favorite = ' favorite';
			}
			
			var $li = $('<li><span>' + category.name + '<i class="fas fa-star'+favorite+'"></i></span></li>')
						.click(function(e){
							e.stopPropagation();
							drawProducts(category.id);
						});
						
			$('i.fas', $li).click(function(e){
				e.preventDefault();
				_toggleFavorite(id_category, $(this).hasClass('favorite'));
				$(this).toggleClass('favorite');
			});
		
			if(category.subcategories.length > 0)
			{
				//il y a des sous-categories
				var $ul = $('<ul></ul>');
				
				
				$.each(category.subcategories,function(i,id_category){
					_drawNode(id_category, $ul);
				});
				
				$li.append($ul);
			}			
			
			$container.append($li);
			
		}
		
	};
	
	var drawProducts = function(id_category) {
		
		var products = [];
		
		if(navigator.onLine) //je suis en ligne
		{
			products = loadProductsByIdCategory(id_category);

		}
		else //pas de connection
		{
			//est-ce une category en favori?
			if(localStorage.getItem('fav_' + id_category) != null)
			{
				//en favori
				var category = _getCategoryFromLocalStorage(id_category);
				
				$.each(category.products,function(i,id_product){
					var product = localStorage.getItem('prod_' + id_product);
					
					if(product != null)
					{
						products.push(JSON.parse(product));
					}
					
					
				});
				
			}
			else
			{
				//pas en favori
				products = false;
			}
		}
		
		//ici faudra réaliser la génération de l'html
		$('#product-list').html(generateProductListHTML(products));
		
	};
	
	var generateProductListHTML = function(products) {
		
		if(products === false)
		{
			return '<div class="error">Impossible d\'afficher le contenu sans conexion à internet</div>'; 
		}
		
		var html = '<ul>';
		
		console.log(products);
		
			$.each(products,function(i,product){
				if(product == null)
				{
					return;
				}
				html += '<li>';
				html += '<h2>' + product.name + '</h2>';
				html += '<span>' + product.price + '€</span>';
				html += '</li>';
			});
		html += '</ul>';
		
		return html;
	};
	
	var _toggleFavorite = function(id_category, is_favorite)
	{
		if(is_favorite)
		{
			localStorage.removeItem('fav_' + id_category);
			
			//vider localstorage des produits de cette categorie
			
			var category = _getCategoryFromLocalStorage(id_category);
				
			$.each(category.products,function(i,id_product){
				//finition, il faudrait tester si ce produit n'est pas présent dans une autre catégorie encore en favori
				localStorage.removeItem('prod_' + id_product);
			});
		}
		else
		{
			if(navigator.onLine === true) //je suis en ligne
			{
				localStorage.setItem('fav_' + id_category, 1);
				storeInCacheProductsByIdCategory(id_category);
			}
			else
			{
				alert('Désolé, vous devez être connecté pour mettre en favori cette catégorie');
			}
			
			
		}
	};
	
	var storeInCacheProductsByIdCategory = function(id_category) {
		var products = loadProductsByIdCategory(id_category);
		
		$.each(products,function(i,product) {
			
			localStorage.setItem('prod_' + product.id, JSON.stringify(product));
			
		});
		
	};
	
	var loadProductsByIdCategory = function(id_category)
	{
		console.log('loadProductsByIdCategory('+id_category+')');
		
		var products;
		
		$.ajax({
			url : URL_PROXY,
			data : {
				action : 'productByCategoryId',
				id_category : id_category
			},
			async: false,	
		}).done(function(response){
			console.log(response);
			products = response.products;
			
			
		}).fail(function(){
			alert('Proxy injoignable');
		});
		
		return products;
		
	};
	
	
	
	var _getCategoryFromLocalStorage = function(id_category)
	{
		var category = localStorage.getItem('cat_' + id_category);
		
		if(category == null)
		{
			return false;
		}
		else
		{
			return JSON.parse(category);
		}
	};
	
	
	

});