<?php
header('Content-Type: application/json');

error_reporting(E_ALL);
ini_set('display_errors', 1);


define('WEBSERVICE_URL','http://golivier.srv5.formations-web.alsace/presta17/api/');
define('API_KEY','DIXE3SBQ5A4W2K1PXVBTAYTU8K6SNYR1');

/**
 * Pour obtenir l'intégralité de l'arborescence
 */
function getTree()
{

	$url = WEBSERVICE_URL.'categories/?display=full';


	// create curl resource 
	$ch = curl_init(); 
	
	// set url 
	curl_setopt($ch, CURLOPT_URL, $url); 
	
	//set basic authentification
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, API_KEY . ':');
	
	//return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	
	// $output contains the output string 
	$output = curl_exec($ch); 
	
	// close curl resource to free up system resources 
	curl_close($ch);     
	
	$xml = new SimpleXMLElement($output); 
	
	//echo('<pre>');
	//print_r($xml);die();
	
	$categories = array();
	
	foreach($xml->categories->category as $category)
	{
		
		
		
		$id = (int)$category->id->__toString();
		$id_parent = (int)$category->id_parent->__toString();
		$position = (int)$category->position->__toString();
		$level_depth = (int)$category->level_depth->__toString();
		$name = $category->name->language->__toString();
		
		$products = array();
		
		if(isset($category->associations->products->product))
		{
			//print_r($category->associations->products->product);
			foreach($category->associations->products->product as $product)
			{
				$products[] = (int)$product->id->__toString();
			}
		}
		
		
		$subcategories = array();
		
		if(isset($category->associations->categories->category))
		{
			//print_r($category->associations->products->product);
			foreach($category->associations->categories->category as $subcategory)
			{
				$subcategories[] = (int)$subcategory->id->__toString();
			}
		}
		
		$categories[] = array(
			'id' => $id,
			'name' => $name,
			'id_parent' => $id_parent,
			///'position' => $position,
			//'level_depth' => $level_depth,
			'products' => $products, 
			'subcategories' => $subcategories
		);
	}
	
	$result = array(
		'categories' => $categories
	);
	return $result;
}

function getProductByCategoryId($id_category)
{
	$url = WEBSERVICE_URL.'products/?display=full&filter[id_category_default]=[' . $id_category . ']';


	// create curl resource 
	$ch = curl_init(); 
	
	// set url 
	curl_setopt($ch, CURLOPT_URL, $url); 
	
	//set basic authentification
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, API_KEY . ':');
	
	//return the transfer as a string 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	
	
	// $output contains the output string 
	$output = curl_exec($ch); 
	
	// close curl resource to free up system resources 
	curl_close($ch);     
	
	$xml = new SimpleXMLElement($output); 
	
	//echo('<pre>');	print_r($xml);die();
	
	$products = array();
	
	foreach($xml->products->product as $product)
	{
		$products[] = array(
			'id' => $product->id->__toString(),
			'name' => $product->name->language->__toString(),
			'price' => $product->price->__toString(),
		);
	}
	
	return array(
		'products' => $products
	);
}

switch($_GET['action'])
{
	case 'tree' :
		echo(json_encode(getTree()));
		break;
	case 'productByCategoryId' :
		echo(json_encode(getProductByCategoryId($_GET['id_category'])));
		break;
}
	









